//
//  AppDelegate.h
//  kalyanDemo
//
//  Created by Ramoji Krian on 26/07/2560 BE.
//  Copyright © 2560 BE Ramoji Krian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

