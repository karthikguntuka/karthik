//
//  main.m
//  kalyanDemo
//
//  Created by Ramoji Krian on 26/07/2560 BE.
//  Copyright © 2560 BE Ramoji Krian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
