//
//  ViewController.m
//  kalyanDemo
//
//  Created by Ramoji Krian on 26/07/2560 BE.
//  Copyright © 2560 BE Ramoji Krian. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (strong, nonatomic) IBOutlet UILabel *emailLbl;
@property (strong, nonatomic) IBOutlet UILabel *passwordLbl;
@property (strong, nonatomic) IBOutlet UIButton *submitBtn;


@end

@implementation ViewController
@synthesize emailTF,passwordTF,view1,view2,submitBtn;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    emailTF.delegate=self;
    passwordTF.delegate=self;
    
    submitBtn.layer.cornerRadius = 5.0f;
    submitBtn.clipsToBounds = YES;
    
    //kfjgdfgjdfgdfjdfkljgdljhdlkfjgd
    
    // suman changes....
    
    
    // uisng version controller from the my IDE suman changes
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    return NO;
}

- (BOOL)validateEmailWithString:(NSString*)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (IBAction)submitBtn:(id)sender {
    
    if (![self validateEmailWithString:emailTF.text])
    {
        [_emailLbl setHidden:NO];

        
        
    }else if ([passwordTF.text isEqualToString:@""])
    {
        [_passwordLbl setHidden:NO];
        
    }
        
    
}
@end
